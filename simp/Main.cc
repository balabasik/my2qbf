/*****************************************************************************************[Main.cc]
Copyright (c) 2003-2006, Niklas Een, Niklas Sorensson
Copyright (c) 2007,      Niklas Sorensson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************************************/

#include <errno.h>

#include <signal.h>
#include <zlib.h>
#include <sys/resource.h>

#include "utils/System.h"
#include "utils/ParseUtils.h"
#include "utils/Options.h"
#include "core/Dimacs.h"
#include "core/Valera.h"
#include "simp/SimpSolver.h"
#include <set>

using std::set;
using std::vector;
using namespace Minisat;

//=================================================================================================

/*static Solver* solver;
// Terminate by notifying the solver and back out gracefully. This is mainly to have a test-case
// for this feature of the Solver as it may take longer than an immediate call to '_exit()'.
static void SIGINT_interrupt(int signum) { solver->interrupt(); }

// Note that '_exit()' rather than 'exit()' has to be used. The reason is that 'exit()' calls
// destructors and may cause deadlocks if a malloc/free function happens to be running (these
// functions are guarded by locks for multithreaded use).
static void SIGINT_exit(int signum) 
	{
    printf("\n"); 
    printf("*** INTERRUPTED ***\n");
    _exit(1); 
    }*/

//=================================================================================================
// Main:

int main(int argc, char** argv)
	{
    try 
    	{
        setUsageHelp("USAGE: %s [options] <input-file> <result-output-file>\n\n  where input may be either in plain or gzipped DIMACS.\n");

		//#if defined(__linux__)
        //fpu_control_t oldcw, newcw;
        //_FPU_GETCW(oldcw); newcw = (oldcw & ~_FPU_EXTENDED) | _FPU_DOUBLE; _FPU_SETCW(newcw);
        //printf("WARNING: for repeatability, setting FPU to use double precision\n");
		//#endif
		
        // Extra options:
        //--------------------------------------------------------------------------------
        IntOption    verb     ("MAIN", "verb",   "Verbosity level (0=silent, 1=some, 2=more).", -1, IntRange(-1, 2));
        BoolOption   epre     ("MAIN", "epre",    "Use preprocessing for existential solver.", false);
        BoolOption   upre     ("MAIN", "upre",    "Use preprocessing for universal solver", false);
        BoolOption   usesub   ("MAIN", "usesub", "Use subsumption simplification.", true);
        BoolOption   satguide ("MAIN", "satguide",    "Use SAT based guide", false);                
        BoolOption   useguide ("MAIN", "useguide", "Use guided SAT solving.", false);
        StringOption dimacs   ("MAIN", "dimacs", "If given, stop after preprocessing and write the result to this file.");
        IntOption    cpu_lim  ("MAIN", "cpu-lim","Limit on CPU time allowed in seconds.\n", INT32_MAX, IntRange(0, INT32_MAX));
        IntOption    mem_lim  ("MAIN", "mem-lim","Limit on memory usage in megabytes.\n", INT32_MAX, IntRange(0, INT32_MAX));
		//--------------------------------------------------------------------------------
        parseOptions(argc, argv, true);
        //--------------------------------------------------------------------------------		
        // Set limit on CPU-time:
        if (cpu_lim != INT32_MAX){
            rlimit rl;
            getrlimit(RLIMIT_CPU, &rl);
            if (rl.rlim_max == RLIM_INFINITY || (rlim_t)cpu_lim < rl.rlim_max){
                rl.rlim_cur = cpu_lim;
                if (setrlimit(RLIMIT_CPU, &rl) == -1)
                    printf("WARNING! Could not set resource limit: CPU-time.\n");
            } }
        // Set limit on virtual memory:
        if (mem_lim != INT32_MAX){
            rlim_t new_mem_lim = (rlim_t)mem_lim * 1024*1024;
            rlimit rl;
            getrlimit(RLIMIT_AS, &rl);
            if (rl.rlim_max == RLIM_INFINITY || new_mem_lim < rl.rlim_max){
                rl.rlim_cur = new_mem_lim;
                if (setrlimit(RLIMIT_AS, &rl) == -1)
                    printf("WARNING! Could not set resource limit: Virtual memory.\n");
            } }
        if (argc == 1)
            printf("Reading from standard input... Use '--help' for help.\n");
        gzFile in = (argc == 1) ? gzdopen(0, "rb") : gzopen(argv[1], "rb");
        if (in == NULL)
            printf("ERROR! Could not open file: %s\n", argc == 1 ? "<stdin>" : argv[1]), exit(1);                                                       
        //--------------------------------------------------------------------------------
        //-------------------------------------------------------------------------------- 
        printf("\n----------------------------------------------------------\n");               
        double initial_time = cpuTime();
        
        SimpSolver eSolver, uSolver;
        
        if (!epre) {eSolver.eliminate(true);}
        if (!upre) {uSolver.eliminate(true);}
        
        vector<vector<int> > epoccs, enoccs, upoccs, unoccs;   // positive, negative, universal and existential occurrence lists;
        vector<vector<int> > subsum, ucnf, ecnf;               // subsumption info, universal and existential CNF containers
        vector<int> inversemap, clausemap, unitmap;            // maps of original var naming, clause names (for unique uCls), activation variables names (for unit detection)
        int nevars, nuvars, nunits;                            // number of exist and unit vars ; number of unit clauses
        vector<int> dfrequency;								   // shows how many the same d's we have	
        
        parse_DIMACS(in, inversemap, nevars, nuvars, ucnf, ecnf);
        gzclose(in);
        
        double parse_time = cpuTime();
        printf("Parsing done. UVars: %d EVars: %d Cls: %d Time: %f\n", nuvars, nevars, (int)ecnf.size(), parse_time-initial_time);
        //--------------------------------------------------------------------------------
        //-------------------------------------------------------------------------------- 
        // NOTE: we assume subsum and units are ON       
		BuildSolvers(ucnf, ecnf, nuvars, nevars, uSolver, eSolver, subsum, upoccs, unoccs, epoccs, enoccs, clausemap, unitmap, nunits, dfrequency);
        
        /*for (int i=0;i<nuvars;i++)
        	{
        	printf("%d : ", i);
        	for (int j=0;j<unoccs[i].size();j++) {printf("%d ",unoccs[i][j]);}
        	printf("\n");
        	}
        
        for (int i=0;i<nuvars;i++)
        	{
        	printf("%d : ", i);
        	for (int j=0;j<upoccs[i].size();j++) {printf("%d ",upoccs[i][j]);}
        	printf("\n");
        	}
        	
        for (int i=0;i<ucnf.size();i++)
        	{
        	printf("clause %d %d : ", i, clausemap[i]);
        	for (int j=0;j<ucnf[i].size();j++) {printf("%d ", ucnf[i][j]);}
        	printf("\n");
        	}*/
        
        double building_time = cpuTime();
        printf("Building solvers done. Unique: %d Units: %d Time: %f\n", (int)unitmap.size(), nunits, building_time - parse_time);
		//--------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------
		//FIXME: elimination of unit clauses??
		if (epre) 
			{
			for (int i=0;i<nuvars;i++)  {eSolver.setFrozen(i+nevars,true);}	
			eSolver.eliminate(true);
			}
		if (upre)
			{
			for (unsigned i=0;i<unitmap.size();i++) {uSolver.setFrozen(abs(unitmap[i])-1,true);}
			uSolver.eliminate(true);
			}
		if (upre || epre)
			{
			double pre_time = cpuTime();
        	printf("Preprocessing done. Ucls: %d Ecls: %d Time: %f\n", uSolver.nClauses(), eSolver.nClauses(), pre_time - building_time);
        	}
        //--------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------
		//double main_time = cpuTime();
		
		vec<Lit> udummy, edummy, lits;
		lbool uret = l_False;
		lbool eret = l_False;
		
		int goodneighbors = 0;
		double etime = 0, utime = 0;
		
		for (int i=nevars;i<nevars+nuvars;i++) {edummy.push(mkLit(i,false));}
	
		vector<bool> emodel,umodel;
		vector<vector<int> > dblock;
		//set<int> dset;
		map<int,int> dset, dset_copy;
		
		double bbtime = 0, iitime = 0, badguidetime = 0, blocktime = 0, totalguidetime = 0;
		
		for (int i=0;i<nuvars;i++) {umodel.push_back(false);}
		for (int i=0;i<nevars;i++) {emodel.push_back(false);}
		
		set<int> added,removed;
		
		int jcounter = 0, ucounter = 0;
		
		vector<vector<int> > doccs;
		vector<int> dcex, dcex_copy;
		vector<int> empty;
	
		for (unsigned i=0;i<unitmap.size();i++) {doccs.push_back(empty);}
		
		//NOTE: used to only flip unit vars 
		set<int> unitvars;	
		for (unsigned i=0;i < unitmap.size();i++) 
			{
			if (abs(unitmap[i])<=nuvars) {unitvars.insert(abs(unitmap[i])-1);}
			}
        vec<Lit> sdummy;
        for (int i=0;i<nuvars;i++) {sdummy.push(mkLit(i));}


		// THE MAIN CEGAR QBF LOOP
		//================================================================================
		int iteration = 0;
		while (true)
			{//if (iteration>10) {break;}
			if (verb>=0) {printf(" - iteration: %d  ", iteration);}
			
			bool goodneighbor = false;
			
			double guidetime = cpuTime();
			if (!satguide && useguide && iteration>0)
				{
				//bbtime -= cpuTime();
				//if (iteration==1) {BuildDSet(upoccs,unoccs,umodel,dset);}
				//bbtime += cpuTime();
		
				dcex.push_back(-1);				
				int badindex = -1;
				
				//FIXME: here we don't need to flip all literals, only a subset maybe!!
				//NOTE: flip only unit vars??
				for (set<int>::iterator i=unitvars.begin();i!=unitvars.end();++i)
				//for (unsigned i=0;i<umodel.size();i++)
					{
					ucounter++;
					//int uindex = (umodel[i] ? i+1 : -(i+1));
					int uindex = (umodel[*i] ? *i+1 : -(*i+1));
					
					//FIXME: need to optimize this part!! (don't copy, but update)
					dset_copy = dset;
					dcex_copy = dcex;
				
					// NOTE: optimized version!!!!
					bbtime -= cpuTime();
					UpdateDSet(upoccs,unoccs,uindex,dset_copy,added,removed);
					bbtime += cpuTime();

					/*printf("dset: ");
					for (map<int,int>::iterator it=dset_copy.begin();it!=dset_copy.end();++it) {printf("%d ",it->first);}
					printf("0\n");
					
					printf("    added: ");
					for (set<int>::iterator it=added.begin();it!=added.end();++it) {printf("%d ",*it);}
					printf("0\n");
					
					printf("    removed: ");
					for (set<int>::iterator it=removed.begin();it!=removed.end();++it) {printf("%d ",*it);}
					printf("0\n");
					
					printf("    lastblock: ");
					for (set<int>::iterator it=dblock.back().begin();it!=dblock.back().end();++it) {printf("%d ",*it);}
					printf("0\n");*/
					
					iitime -= cpuTime();
					set<int>::iterator inter_it = empty_intersection(removed.begin(),removed.end(),dblock.back().begin(),dblock.back().end());
				
					if (inter_it == removed.end())
						{
						goodneighbor = false;
						iitime += cpuTime();
						continue;
						}
					else {dcex_copy.back() = *inter_it;}// printf("  added cex: %d\n",dcex_copy.back());}
				
					if (badindex!=-1)
						{
						jcounter++;
						vector<int>::iterator mapit = map_includes(dset_copy.begin(),dset_copy.end(),dblock[badindex].begin(),dblock[badindex].end());
						if (mapit==dblock[badindex].end())
							{
							goodneighbor = false;
							iitime += cpuTime();
							continue;
							}
						}
						
					set<int> dqueu;
					for (set<int>::iterator it = added.begin();it!=added.end();++it) 
						{
						for (unsigned j=0;j<doccs[*it].size();j++) {dqueu.insert(doccs[*it][j]);}
						}						
						
					//printf("dset: ");
					//for (map<int,int>::iterator it=dset.begin();it!=dset.end();++it) {printf("%d ",it->first);}
					//printf("0\n");	
						
					goodneighbor = true;		
					for (set<int>::iterator it = dqueu.begin();it!=dqueu.end();++it)
						{
						/*printf("  dblock %d : ", *it);
						for (set<int>::iterator it1=dblock[*it].begin();it1!=dblock[*it].end();++it1) {printf("%d ",*it1);}
						printf("0\n");
						
						printf("  cex: %d\n", dcex_copy[*it]);*/
						
						assert(dcex_copy.size()==dblock.size());
						assert(dcex_copy[*it]!=-1);
						if (added.find(dcex_copy[*it])==added.end()) 
							{
							//printf("-----here-----\n");
							continue;
							}
						
						jcounter++;
						vector<int>::iterator mapit = map_includes(dset_copy.begin(),dset_copy.end(),dblock[*it].begin(),dblock[*it].end());
						if (mapit==dblock[*it].end())
							{
							goodneighbor = false;
							badindex= *it;
							break;
							}
						else {dcex_copy[*it] = *mapit;}
						}
				
					iitime += cpuTime();
				
					if (goodneighbor)
						{//printf("goodneighbor\n");
						if (verb>0) printf("1");
						goodneighbors++;
						//umodel[i] = !umodel[i];
						umodel[*i] = !umodel[*i];
						dset = dset_copy;
						dcex = dcex_copy;
						break;
						}
					}
				}

			if (satguide && useguide && iteration>0)
                {
                for (set<int>::iterator i=unitvars.begin();i!=unitvars.end();++i)
                    {
                    ucounter++;
                    sdummy[*i]=~sdummy[*i];
                    lbool sret = uSolver.solveLimited(sdummy);
                    if (sret == l_True) 
                        {
                        goodneighbor = true;
                        goodneighbors++;
                        umodel[*i]=!umodel[*i];
                        break;
                        }
                    sdummy[*i]=~sdummy[*i];
                    }
                }

            guidetime = cpuTime() - guidetime;
            totalguidetime+=guidetime;

            if (!goodneighbor) 
				{
				badguidetime += guidetime;
				if (verb>0) printf("0");
				utime -= cpuTime();
				uret = uSolver.solveLimited(udummy);
				utime += cpuTime();
				
				if (uret != l_True) 
					{
					if (verb>=0) {printf("\n");}
					break;
					}
				for (int i=0;i<nuvars;i++) 
                    {
                    if (sign(sdummy[i])==(uSolver.model[i]==l_True)) {sdummy[i]=~sdummy[i];}
                    umodel[i] = (uSolver.model[i]==l_True);
                    }
				
				//NOTE: we need to build dset at this point
				if (useguide && !satguide)
					{
					bbtime -= cpuTime();
					BuildDSet(upoccs,unoccs,umodel,dset);
					for (unsigned i=0;i<unitmap.size() && dcex.size()>0;i++)
						{
						if ((uSolver.model[abs(unitmap[i])-1]==l_False)!=(unitmap[i]>0)) {continue;}
						for (unsigned j=0;j<doccs[i].size();j++) {dcex[doccs[i][j]] = i;}
						}
					bbtime += cpuTime();
					}
				}

			if (verb>0) {printf("\n      usolver  [true]: ");}
			
			for (int i=0;i<nuvars;i++) 
				{
				edummy[i] = mkLit(var(edummy[i]),!umodel[i]);
				if (verb>0)
					{
					printf(umodel[i] ? "" : "-");
					printf("%d ", inversemap[i]+1);
					}
				}
			
			if (verb>0) {printf("\n");}
			
			etime -= cpuTime();
			eret = eSolver.solveLimited(edummy);
			etime += cpuTime();

			if (eret != l_True) {break;}
			
			if (verb>0) 
				{
				printf("      esolver  [true]: ");
				for (int i=0;i<nevars;i++) 
					{
					printf((eSolver.model[i]==l_True ? "" : "-"));
					printf("%d ", inversemap[i + nuvars]+1);
					}
				printf("\n");
				}
			
			for (int i=0;i<nevars;i++) {emodel[i] = (eSolver.model[i] == l_True);}
			blocktime-=cpuTime();
			BlockCofactor(emodel,epoccs,enoccs,clausemap,unitmap,subsum,uSolver,usesub,verb,dblock,doccs,useguide,dfrequency);	
			blocktime+=cpuTime();
			iteration++;
			}
		
		printf("jcounter: %d / %d Bad time: %f Block time: %f\n", jcounter, ucounter, badguidetime, blocktime );
		
		assert(!(uret == l_False && eret == l_False));
		assert(!(uret == l_True && eret == l_True));
		
		//printf("savesolvetime: %f trytime: %f nsolves: %d ntrue: %d \n", savesolvetime, trytime, nsavesolves, ncandidates);
		printf("ETime: %f UTime: %f \nGTime: %f = IITime: %f + BBTime: %f \n", etime, utime, totalguidetime, iitime, bbtime);
		printf("Finished. Iters: %d Guide: %d Time: %f\n", iteration,goodneighbors,cpuTime()-initial_time);
		
		if (uret == l_False || eret == l_True) {printf("QBF is TRUE\n");}
		else if (uret == l_True || eret == l_False) {printf("QBF is FALSE\n");}
		else {printf("QBF is UNKNOWN\n");}
		printf("----------------------------------------------------------\n\n"); 
    	return 0;
		} 
	catch (OutOfMemoryException&)
		{
        printf("===============================================================================\n");
        printf("INDETERMINATE\n");
        exit(0);
    	}
	}
