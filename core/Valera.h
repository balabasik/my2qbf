#ifndef Minisat_Valera_h
#define Minisat_Valera_h

#include "mtl/Vec.h"
#include "simp/SimpSolver.h"
#include "core/SolverTypes.h"

#include <stdio.h>
#include <algorithm>
#include <vector>
#include <set>
#include <map>

using namespace std;
using namespace Minisat;	

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
void SubReduce(const vector<vector<int> >& subsum, set<int>& litset) 
	{
	vector<int> subactive;
	for (unsigned i=0;i<subsum.size();i++) {subactive.push_back(false);}
	for (set<int>::iterator it = litset.begin(); it!=litset.end(); ++it) {subactive[*it]=true;}

	for (unsigned i=0;i<subactive.size();i++)
		{
		if (!subactive[i]) {continue;}
		for (unsigned j=0;j<subsum[i].size();j++) {subactive[subsum[i][j]] = false;}
		}
		
	litset.clear();
	for (unsigned i=0;i<subactive.size();i++) 
		{
		if (subactive[i]) {litset.insert(i);}
		}
	}	

void SubReduceFreq(const vector<vector<int> >& subsum,vector<int>& dfrequency)
	{
	assert(subsum.size()==dfrequency.size());
	for (unsigned i=0;i<subsum.size();i++)
		{
		assert(dfrequency[i]>=0);
		if (dfrequency[i]==0) {continue;}
		for (unsigned j=0;j<subsum[i].size();j++) {dfrequency[subsum[i][j]] = 0;}
		}
	}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------	
void BlockCofactor(vector<bool>& model, vector<vector<int> >& poccs, vector<vector<int> >& noccs, const vector<int>& clausemap, const vector<int>& unitmap, const vector<vector<int> >& subsum, SimpSolver& uSolver, bool usesub, int verb, vector<vector<int> >& dblock, vector<vector<int> >& doccs, bool useguide, vector<int> dfrequency) 
	{
	vector<bool> active;
	int nevars = model.size();
	vector<int> litset;    
	        
	//FIXME: need to make this part more efficient!
	for (unsigned i=0;i<clausemap.size();i++) {active.push_back(true);}			
	for (int i=0;i<nevars;i++)
		{
		if (model[i])
			{
			for (unsigned j=0;j<poccs[i].size();j++)
				{
				if (active[poccs[i][j]]) 
					{
					active[poccs[i][j]]=false;
					dfrequency[clausemap[poccs[i][j]]]--;
					}
				}
			}
		else
			{
			for (unsigned j=0;j<noccs[i].size();j++)
				{
				if (active[noccs[i][j]]) 
					{
					active[noccs[i][j]]=false;
					dfrequency[clausemap[noccs[i][j]]]--;
					}
				}
			}	
		}
		
	/*for (unsigned i=0;i<active.size();i++)
		{
		if (active[i]) {litset.insert(clausemap[i]);}
		}
	
	int beforesubsize = litset.size();
	if (usesub) {SubReduce(subsum, litset);}
			
	vec<Lit> lits;
	for (set<int>::iterator it = litset.begin(); it!=litset.end(); ++it) 
		{
		lits.push(mkLit(abs(unitmap[*it])-1,unitmap[*it]>0));
		if (useguide) {doccs[*it].push_back(dblock.size());}
		}*/
		
	if (usesub) {SubReduceFreq(subsum,dfrequency);}	
	
	vec<Lit> lits;
	for (unsigned i=0;i<dfrequency.size();i++)
		{
		assert(dfrequency[i]>=0);
		if (dfrequency[i]==0) {continue;}
		lits.push(mkLit(abs(unitmap[i])-1,unitmap[i]>0));
		if (useguide) 
			{
			doccs[i].push_back(dblock.size());
			litset.push_back(i);
			}
		}
		
	uSolver.addClause(lits);
	
	if (useguide) 
		{
		sort(litset.begin(),litset.end());
		dblock.push_back(litset);
		}
	if (verb==0) {printf("  %d / %d \n", (int)unitmap.size(),(int)lits.size());}
	}
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
int CheckInclusion(const vector<vector<int> >& cnf,const vector<int>& unique_ind, const vector<int>& clause) {

for (unsigned i=0;i<unique_ind.size();i++)
	{
	if (clause.size()!=cnf[unique_ind[i]].size()) {continue;}
	bool equal = true;
	for (unsigned j=0;j<clause.size();j++)
		{
		if (clause[j]!=cnf[unique_ind[i]][j]) {equal=false;break;}
		}
	if (equal) {return i;}
	}

return -1;
}
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
/*  (a,b,c) subsumes (a,c) and (b,c)*/
bool FirstSubsumesSecond(const vector<int>& clause1,const vector<int>& clause2) {

if (clause1.size()<clause2.size()) {return false;}

int index = 0;

for (unsigned i=0;i<clause2.size();i++)
	{
	bool found = false;
	for (int j=index;j<(int)clause1.size();j++)
		{
		if (clause2[i]==clause1[j])
			{
			found = true;
			index = j+1;
			break;
			}
		}
	if (!found) {return false;}
	}
	
//NOTE: all the clauses are distinct actually at this point
assert(clause1.size()!=clause2.size());
return true;
}
// NOTE: actually its more as backsubsumption: for each clause we have list of clauses that subsume this clause
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
int BuildSubsum(const vector<vector<int> >& cnf,const vector<int>& unique_ind, vector<vector<int> >& subsum) {

vector<int> dummy;

for (unsigned i=0;i<unique_ind.size();i++) {subsum.push_back(dummy);}
for (int i=0;i<(int)unique_ind.size();i++)
	{
	for (int j=0;j<(int)unique_ind.size();j++)
		{
		if (i==j) {continue;}
		if (FirstSubsumesSecond(cnf[unique_ind[j]],cnf[unique_ind[i]])) {subsum[i].push_back(j);}
		}
	//NOTE: all clauses subsume the empty clause
	assert(cnf[unique_ind[i]].size()!=0 || subsum[i].size()==(unique_ind.size()-1));
	}

return -1;
}		
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
template<class Solver>
void BuildSolvers(
const vector<vector<int> >& ucnf, 
const vector<vector<int> >& ecnf, 
int nuvars, 
int nevars, 
Solver& uS, 
Solver& eS, 
vector<vector<int> >& subsum, 
vector<vector<int> >& upoccs,
vector<vector<int> >& unoccs,
vector<vector<int> >& epoccs,
vector<vector<int> >& enoccs,
vector<int>& clausemap, 
vector<int>& unitmap, 
int& units, 
vector<int>& dfrequency
) {
	vec<Lit> elits, ulits;
    vector<int> dummy;
    int notunits = 0;
    bool usesub = true;
    
    vector<int> unique_clauses;
    
    units = 0;
    
    for (int i=0;i<nuvars;i++) {upoccs.push_back(dummy);unoccs.push_back(dummy);}
    for (int i=0;i<nevars;i++) {epoccs.push_back(dummy);enoccs.push_back(dummy);}
    
    for (int i=0;i<ucnf.size();i++)
    	{    
        int index = CheckInclusion(ucnf,unique_clauses,ucnf[i]);
        //assert(index <= i);
        
        bool unique = (index==-1);
    	bool unit = (ucnf[i].size()==1);
            
        if (unique) 
            {
            index = unique_clauses.size();
            if (unit) 
            	{
            	unitmap.push_back(ucnf[i][0]);
            	units++;
            	}
            else 
            	{
            	unitmap.push_back(notunits+nuvars+1);
            	notunits++;
            	}
            unique_clauses.push_back(i);
			}
			
		clausemap.push_back(index);
		while (dfrequency.size()<=index) {dfrequency.push_back(0);}
		dfrequency[index]++;
            
        elits.clear();
        ulits.clear();
            
        // NOTE: filling in the existential solver
        for (int j=0;j<ecnf[i].size();j++)
            {
            int evar = ecnf[i][j];
            
            if (evar>0) {epoccs[abs(evar)-1].push_back(i);}
            else        {enoccs[abs(evar)-1].push_back(i);}

            while (abs(evar)-1 >= eS.nVars()) {eS.newVar();}
            elits.push(mkLit(abs(evar)-1,evar<0));
            }
                	
        for (int j=0;j<ucnf[i].size();j++)
            {
            int uvar = (abs(ucnf[i][j]) + nevars)*(ucnf[i][j] > 0 ? 1 : -1); 
            while (abs(uvar)-1 >= eS.nVars()) {eS.newVar();}
            elits.push(mkLit(abs(uvar)-1,uvar<0));
            }
        
        eS.addClause(elits);

		if (unique)
			{
			for (int j=0;j<ucnf[i].size();j++)
            	{
           		if (ucnf[i][j]>0) {upoccs[abs(ucnf[i][j])-1].push_back(index);}
            	else        {unoccs[abs(ucnf[i][j])-1].push_back(index);}
				}
			}
			
        if (!unique || unit) {continue;}
				
		// NOTE: filling in the universal solver   
        int dvar = abs(unitmap.back())-1;         
        for (int j=0;j<ucnf[i].size();j++)
            {
            ulits.clear();
            ulits.push(mkLit(abs(ucnf[i][j])-1,ucnf[i][j]>0));
            ulits.push(mkLit(dvar,false));
            while (abs(ucnf[i][j])-1 >= uS.nVars() || dvar >= uS.nVars()) {uS.newVar();}
            uS.addClause(ulits);
			}
		}
	// In case if all clauses were units
	while (nuvars > uS.nVars()) {uS.newVar();}
	if (usesub) {BuildSubsum(ucnf,unique_clauses,subsum);}
	}
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------		
void BuildDSet(const vector<vector<int> >& poccs, const vector<vector<int> >& noccs, const vector<bool>& model, map<int,int>& dset) 
	{
	dset.clear();
	assert(model.size()==poccs.size());
	
	for (unsigned i=0;i<model.size();i++)
		{ 
		if (model[i]) 
			{
			for (unsigned j=0;j<poccs[i].size();j++) 
				{
				//dset.insert(poccs[i][j]);
				dset[poccs[i][j]]++;
				}
			}
		else 
			{
			for (unsigned j=0;j<noccs[i].size();j++) 
				{
				//dset.insert(noccs[i][j]);
				dset[noccs[i][j]]++;
				}
			}
		}
	}	
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------	
void UpdateDSet(const vector<vector<int> >& upoccs, const vector<vector<int> >& unoccs, int uindex, map<int,int>& dset, set<int>& added, set<int>& removed) 
	{
	//NOTE: positive uindex means we flipped positive literal into negative one
	int var = abs(uindex) - 1; 
	added.clear();
	removed.clear();
	
	for (unsigned i=0;i<upoccs[var].size();i++)
		{
		int ind = upoccs[var][i];
		assert(uindex < 0 || dset.find(ind)!=dset.end());
		//NOTE: if new element is inserted its second value is initialized to 0
		dset[ind] += (uindex>0 ? -1 : +1 ); 
		if (uindex>0 && dset[ind]==0) {dset.erase(ind);removed.insert(ind);}
		if (uindex<0 && dset[ind]==1) {added.insert(ind);}
		}

	for (unsigned i=0;i<unoccs[var].size();i++)
		{
		int ind = unoccs[var][i];
		assert(uindex > 0 || dset.find(ind)!=dset.end());
		//NOTE: if new element is inserted its second value is initialized to 0
		dset[ind] += (uindex<0 ? -1 : +1 ); 
		if (uindex<0 && dset[ind]==0) {dset.erase(ind);removed.insert(ind);}
		if (uindex>0 && dset[ind]==1) {added.insert(ind);}
		}
	}	
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
// iterator to a common element in SET1
template <class InputIterator1, class InputIterator2>
InputIterator1 empty_intersection (InputIterator1 first1, InputIterator1 last1, InputIterator2 first2, InputIterator2 last2)
	{
    while (first1!=last1 && first2!=last2)
    	{
    	if (*first1<*first2) ++first1;
    	else if (*first2<*first1) ++first2;
    	else {break;}
  		}
  	return (first2==last2 ? last1 : first1);
	}
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//returns iterator to element in SET2 not present in MAP1
template <class MapIterator1, class InputIterator2>
InputIterator2 map_includes (MapIterator1 first1, MapIterator1 last1, InputIterator2 first2, InputIterator2 last2)
	{
    while (first2!=last2) 
    	{
    	if ( (first1==last1) || (*first2<first1->first) ) {break;}
    	if (!(first1->first<*first2)) ++first2;
    	++first1;
  		}
  	return first2;
	}
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
#endif