/****************************************************************************************[Dimacs.h]
Copyright (c) 2003-2006, Niklas Een, Niklas Sorensson
Copyright (c) 2007-2010, Niklas Sorensson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************************************/

#ifndef Minisat_Dimacs_h
#define Minisat_Dimacs_h

#include <stdio.h>
#include <algorithm>

#include "utils/ParseUtils.h"
#include "core/SolverTypes.h"

#include <vector>

using std::vector;
namespace Minisat {
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
template<class B>
static void readClause(B& in, vector<int>& lits) 
	{
    int parsed_lit;
    lits.clear();
    for (;;)
    	{
        parsed_lit = parseInt(in);
        if (parsed_lit == 0) break;
        lits.push_back(parsed_lit);
    	}
	}
//----------------------------------------------------------------------------------------	
//----------------------------------------------------------------------------------------
template<class B>
static void parse_DIMACS_main(B& in, vector<int>& inversemap, int& nevars, int& nuvars, vector<vector<int> >& ucnf, vector<vector<int> >& ecnf) 
	{
    vector<int> map, lits, elits, ulits;
    
    int vars        = 0;
    int clauses     = 0;
    int varcnt      = 0;
    
    bool readforall = false, readexists = false;

    nevars      = 0;
    nuvars      = 0;
    
    for (;;)
    	{
        skipWhitespace(in);
        if (*in == EOF) break;
        else if (*in == 'p')
        	{
            if (eagerMatch(in, "p cnf"))
            	{
                vars    = parseInt(in);
                for (int i=0;i<vars;i++) {map.push_back(-1);}
                clauses = parseInt(in);
            	}
            else
            	{
                printf("PARSE ERROR! Unexpected char: %c\n", *in), exit(3);
            	}
            } 
        else if (*in == 'a')
        	{
        	if (readforall) {printf("PARSE ERROR! Formula is not a 2QBF\n"), exit(3);}
        	readforall = true;
        	++in;
        	readClause(in, lits);
        	for (int i=0;i<lits.size();i++) 
        		{
        		assert(lits[i]>0 && lits[i] <= vars);
        		map[lits[i]-1] = varcnt++;
        		inversemap.push_back(lits[i]-1);
        		nuvars++;
        		}
        	}
		else if (*in == 'e')
        	{
        	if (readexists || !readforall) {printf("PARSE ERROR! Formula is not a 2QBF\n"), exit(3);}
        	readexists = true;
        	++in;
        	readClause(in, lits);
        	for (int i=0;i<lits.size();i++)
        		{
        		assert(lits[i]>0 && lits[i] <= vars);
        		map[lits[i]-1] = varcnt++;
        		inversemap.push_back(lits[i]-1);
        		nevars++;
        		}
        	}
        else if (*in == 'c' || *in == 'p') {skipLine(in);}
        else
        	{
        	elits.clear();
        	ulits.clear();
            readClause(in, lits);
            for (int i=0;i<lits.size();i++) 
            	{
            	int var = map[abs(lits[i])-1] + 1;
            	int sign = (lits[i] > 0 ? 1 : -1);
            	
            	if (var <= nuvars) {ulits.push_back(var*sign);}            // universal
            	else               {elits.push_back((var-nuvars)*sign);}   // existential
            	}
            sort(elits.begin(),elits.end());
            sort(ulits.begin(),ulits.end());
            ecnf.push_back(elits);
            ucnf.push_back(ulits);
            }
        }
    assert (nevars+nuvars == varcnt);
    assert (ecnf.size() == clauses);
	}
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
static void parse_DIMACS(gzFile input_stream, vector<int>& inversemap, int& nevars, int& nuvars, vector<vector<int> >& ucnf, vector<vector<int> >& ecnf) 
	{
    StreamBuffer in(input_stream);
    parse_DIMACS_main(in, inversemap, nevars, nuvars, ucnf, ecnf); 
    }
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
}
#endif
